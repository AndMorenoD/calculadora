package com.example.andre.calculadoranoveno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView panel;

    /**private Button bt0;
    private Button bt1;
    private Button bt2;
    private Button bt3;
    private Button bt4;
    private Button bt5;
    private Button bt6;
    private Button bt7;
    private Button bt8;
    private Button bt9;

    private Button btSuma;
    private Button btResta;
    private Button btDivision;
    private Button btMultiplicacion;
    private Button btPunto;
    private Button btIgual;**/
    private Button acercaDe;

   // private Button btBorrar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        panel = (TextView) findViewById(R.id.nbValores);
        acercaDe = (Button) findViewById(R.id.acerca);

       /** bt0 = (Button) findViewById(R.id.bt0);
        bt1 = (Button) findViewById(R.id.bt1);
        bt2 = (Button) findViewById(R.id.bt2);
        bt3 = (Button) findViewById(R.id.bt3);
        bt4 = (Button) findViewById(R.id.bt4);
        bt5 = (Button) findViewById(R.id.bt5);
        bt6 = (Button) findViewById(R.id.bt6);
        bt7 = (Button) findViewById(R.id.bt7);
        bt8 = (Button) findViewById(R.id.bt8);
        bt9 = (Button) findViewById(R.id.bt9);

        btSuma = (Button) findViewById(R.id.suma);
        btResta = (Button) findViewById(R.id.resta);
        btMultiplicacion = (Button) findViewById(R.id.multiplicacion);
        btDivision = (Button) findViewById(R.id.division);
        btPunto = (Button) findViewById(R.id.btpunto);
        btIgual = (Button) findViewById(R.id.btIgual);**/
        //btBorrar = (Button) findViewById(R.id.btBorrar);

        /**bt0.setOnClickListener(this);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);
        bt5.setOnClickListener(this);
        bt6.setOnClickListener(this);
        bt7.setOnClickListener(this);
        bt8.setOnClickListener(this);
        bt9.setOnClickListener(this);
        btSuma.setOnClickListener(this);
        btResta.setOnClickListener(this);
        btMultiplicacion.setOnClickListener(this);
        btDivision.setOnClickListener(this);
        btPunto.setOnClickListener(this);
        btIgual.setOnClickListener(this);**/
        /**btBorrar.setOnClickListener(this);**/
        acercaDe.setOnClickListener(this);


    }

    public void presionar(View v)
    {
        Button presionado = (Button) v ;
        panel.setText(panel.getText() + presionado.getText().toString());
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.acerca)
        {
            Intent nuevaVentana = new Intent(MainActivity.this, Main2Activity.class );
            startActivity(nuevaVentana);
        }

       /** if(v.getId() == R.id.bt0)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar+"0");
        }

        if(v.getId() == R.id.bt1)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar +"1");
        }

        if(v.getId() == R.id.bt2)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar+"2");
        }

        if(v.getId() == R.id.bt3)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar +"3");
        }
        if(v.getId() == R.id.bt4)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar+"4");
        }

        if(v.getId() == R.id.bt5)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar +"5");
        }
        if(v.getId() == R.id.bt6)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar+"6");
        }

        if(v.getId() == R.id.bt7)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar +"7");
        }
        if(v.getId() == R.id.bt8)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar+"8");
        }

        if(v.getId() == R.id.bt9)
        {
            String concatenar = this.panel.getText().toString();
            panel.setText(concatenar +"9");
        }

        if(v.getId() == R.id.btpunto)
        {
            String concatenar = this.panel.getText().toString();
            if(!panel.getText().toString().equals("")) {
                panel.setText(concatenar + "." );
            }
        }**/

       /**
        if(v.getId() == R.id.btBorrar)
        {
            if (!panel.getText().toString().equals("")){
                panel.setText(panel.getText().subSequence(0,panel.getText().length()-1)+"");
            }
        }**/


    }
}
